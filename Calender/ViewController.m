//
//  ViewController.m
//  Calender
//
//  Created by Click Labs134 on 10/12/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

NSArray *months;
int i;
int a=1;
NSMutableArray *days;
NSMutableArray *dates;
UICollectionViewCell *cell;
NSMutableArray *datesCopy;

@interface ViewController (){
    
}

@property (nonatomic, assign) CGSize headerReferenceSize;
@property (nonatomic, assign) CGSize itemSize;
@property (nonatomic, strong) NSArray *sectionRects;
@property (nonatomic, assign) NSUInteger pageNumber;

@property (strong, nonatomic) IBOutlet UILabel *calenderLabel;
@property (strong, nonatomic) IBOutlet UIButton *addTask;
@property (strong, nonatomic) IBOutlet UIButton *previousMonthButton;
@property (strong, nonatomic) IBOutlet UILabel *currentMonthLabel;
@property (strong, nonatomic) IBOutlet UIButton *nextMonthButton;
@property (strong, nonatomic) IBOutlet UICollectionView *showDatesInCollectionView;


@end

@implementation ViewController

@synthesize calenderLabel;
@synthesize addTask;
@synthesize previousMonthButton;
@synthesize currentMonthLabel;
@synthesize nextMonthButton;
@synthesize showDatesInCollectionView;


- (void)viewDidLoad {
    [super viewDidLoad];
    dates=[[NSMutableArray alloc]init];
    datesCopy=[[NSMutableArray alloc]init];


    
    //Months
    months=[[NSArray alloc]init];
months=@[@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December"];

    
    //insertion of dates
    for (int i = 1 ; i<=31; i++)
    {
        NSString *str = [NSString new];
        str = [NSString stringWithFormat:@"%d",i];
        [dates addObject:str];
    }
    [showDatesInCollectionView reloadData];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section

{
    
    return dates.count ;
    
}



-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 45, 50)];
        label.textColor=[UIColor blackColor];
        label.text = dates[indexPath.row];
        [cell.contentView addSubview:label];
    label.text = [NSString stringWithFormat:@"%@",[dates objectAtIndex:indexPath.row]];
        return cell;
    
}


//next button
- (IBAction)nextMonthButton:(id)sender
{


    
    if (a<=months.count)
    {
        [dates removeAllObjects];
        [showDatesInCollectionView reloadData];
        currentMonthLabel.text=[months objectAtIndex:a];
        if ([currentMonthLabel.text isEqualToString:@"January"]||[currentMonthLabel.text isEqualToString:@"March"]||[currentMonthLabel.text isEqualToString:@"May"]||[currentMonthLabel.text isEqualToString:@"July"]||[currentMonthLabel.text isEqualToString:@"August"]||[currentMonthLabel.text isEqualToString:@"October"]||[currentMonthLabel.text isEqualToString:@"December"])
        {[dates removeAllObjects];
            [showDatesInCollectionView reloadData];
            
            for (int i = 1 ; i<=31; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }
        else if([currentMonthLabel.text isEqualToString:@"February"])
        {
            [dates removeAllObjects];
            [showDatesInCollectionView reloadData];
            for (int i = 1 ; i<=28; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }
        else
        {[dates removeAllObjects];
            [showDatesInCollectionView reloadData];

            for (int i = 1 ; i<=30; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }


    }
    a++;
    
    [showDatesInCollectionView reloadData];
}

//previous button
- (IBAction)previousMonthButton:(id)sender
{
            [dates removeAllObjects];

    if (a > 0)
    {
        a--;
        [dates removeAllObjects];
        [showDatesInCollectionView reloadData];
        currentMonthLabel.text=[months objectAtIndex:a];
        if ([currentMonthLabel.text isEqualToString:@"January"]||[currentMonthLabel.text isEqualToString:@"March"]||[currentMonthLabel.text isEqualToString:@"May"]||[currentMonthLabel.text isEqualToString:@"July"]||[currentMonthLabel.text isEqualToString:@"August"]||[currentMonthLabel.text isEqualToString:@"October"]||[currentMonthLabel.text isEqualToString:@"December"])
        {
            
            for (int i = 1 ; i<=31; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }
        else if([currentMonthLabel.text isEqualToString:@"February"])
        {
            
            for (int i = 1 ; i<=28; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }
        else
        {
            
            for (int i = 1 ; i<=30; i++)
            {
                NSString *str = [NSString new];
                str = [NSString stringWithFormat:@"%d",i];
                [dates addObject:str];
            }
        }
        [showDatesInCollectionView reloadData];
    }
}


@end
